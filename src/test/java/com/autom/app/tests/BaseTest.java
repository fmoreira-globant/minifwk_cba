package com.autom.app.tests;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.asserts.SoftAssert;
import org.testng.internal.TestResult;

import io.github.bonigarcia.wdm.ChromeDriverManager;

public class BaseTest {

	protected WebDriver driver;
	protected SoftAssert softly;

	@BeforeMethod
	public void setUp() throws Exception {
		driver = createDriver();
		softly = new SoftAssert();
		driver.get("http://54.212.7.174/admin");
	}

	@AfterMethod
	public void tearDown() {
		if (driver != null) {
			try {
				driver.quit();
			} catch (Exception e) {
				System.out.println("Warning: did not successfully quit selenium driver.");
			}
		}

		if (softly != null) {
			try {
				softly.assertAll();
			} catch (AssertionError e) {
				Reporter.getCurrentTestResult().setStatus(TestResult.FAILURE);
				Reporter.getCurrentTestResult().setThrowable(e);
			}
		}
	}

	private WebDriver createDriver() throws Exception {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		
		// Disable extensions and hide infobars
        options.addArguments("--disable-extensions");
        options.addArguments("disable-infobars");
        
        // Enable Flash
        Map<String, Object> prefs = new HashMap<>();
        prefs.put("profile.default_content_setting_values.plugins", 1);
        prefs.put("profile.content_settings.plugin_whitelist.adobe-flash-player", 1);
        prefs.put("profile.content_settings.exceptions.plugins.*,*.per_resource.adobe-flash-player", 1);
        
        // Hide save credentials prompt
        prefs.put("credentials_enable_service", false);
        prefs.put("profile.password_manager_enabled", false);
        options.setExperimentalOption("prefs", prefs);
        
        ChromeDriverManager.getInstance().setup();
        driver = new ChromeDriver(options);

		return driver;
	}

}
