package com.autom.app.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import utils.SeleniumUtils;

public class AdminPage extends BasePage{
	
	public static final String INPUT_USERNAME ="user_login";
	public static final String INPUT_PASSWORD ="user_pass";
	public static final String BUTTON_LOGIN = "wp-submit";
	
	public AdminPage(WebDriver driver) {
		super(driver);
	}

	@FindBy(id=INPUT_USERNAME)
	private WebElement inputUserName;
	
	@FindBy(id=INPUT_PASSWORD)
	private WebElement inputPassword;

	@FindBy(id=BUTTON_LOGIN)
	private WebElement buttonLogIn;

	public void login(String username, String password){
		SeleniumUtils.waitForElementToBeVisible(driver, By.id(INPUT_USERNAME), 10);
		inputUserName.sendKeys(username);
		SeleniumUtils.waitForElementToBeVisible(driver, By.id(INPUT_PASSWORD), 10);
		inputPassword.sendKeys(password);
		SeleniumUtils.waitForElementToBeVisible(driver, By.id(BUTTON_LOGIN), 10);
		buttonLogIn.click();
	}

	@Override
	protected By getPageLoadedLocator() {
		return By.id(INPUT_USERNAME);
	}
}
